<!DOCTYPE html >
<html <?php language_attributes(); ?>>
<head>
<!-- Custom Title Setup -->
<title><?php
global $page, $paged;
wp_title( '|', true, 'right' );
// Add the blog name.
bloginfo( 'name' );
// Add the blog description for the home/front page.
$site_description = get_bloginfo( 'description', 'display' );
if ( $site_description && ( is_home() || is_front_page() ) )
    echo " | $site_description";
// Add a page number if necessary:
if ( $paged >= 2 || $page >= 2 )
    echo ' | ' . sprintf( __( 'Page %s', '_s' ), max( $paged, $page ) );
?></title>
<!-- Meta -->
<meta name="description" content="<?php echo get_option('mytheme_description'); ?>" />
<meta content="<?php bloginfo('name'); ?>"/>
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<meta http-equiv="Content-Style-Type" content="text/css" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<!-- Syndication -->
<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<?php echo get_bloginfo_rss('rss2_url'); ?>" />
<link rel="shortcut icon" href="<?php echo esc_url( home_url( '/' ) ); ?>favicon.ico" >
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<link rel="alternate" type="text/xml" title="RSS .92" href="<?php bloginfo('rss_url'); ?>" />
<link rel="alternate" type="application/atom+xml" title="Atom 1.0" href="<?php bloginfo('atom_url'); ?>" />
<!-- Stylesheets -->
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" /> 
<!-- Script -->
<!--[if lt IE 9]>
<script src="<?php bloginfo('stylesheet_directory') ?>/js/selectivizr-min.js"></script>
<![endif]-->
<!-- WP Headers -->
<?php wp_head(); ?>
<style>
#banner {
	background-color: <?php echo stripslashes(get_option('mytheme_bannercolor')); ?>;
}
<?php echo stripslashes(get_option('mytheme_customstyle')); ?>
</style>

</head>
<body class="windsays">
<div class="container">
<div  id="sb-site" class="hfeed site">
	<div id="banner" class="site-header" role="banner">
		 <div class="my-button sb-toggle-right">
			<div class="navicon-line"></div>
			<div class="navicon-line"></div>
			<div class="navicon-line"></div>
		 </div>
		<div class="clear"></div>
		<div id="authorgravatar">
			<div id="gravatartitle" title="<?php echo stripslashes(get_option('mytheme_logotitle')); ?>">
			<img src="<?php bloginfo('stylesheet_directory') ?>/gravatar/me.png">
			</div>
		</div>
		<div class="clear"></div>
		<div class="blogtitle">
			<h1><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" title="<?php bloginfo( 'name' ); ?>"><?php bloginfo( 'name' ); ?></a></h1>

		</div>
		<?php include (TEMPLATEPATH . '/searchform.php'); ?>
	</div>
	<div id="main" class="wrapper">
		<div id="primary" class="site-content">
