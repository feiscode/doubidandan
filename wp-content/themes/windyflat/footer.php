		</div><!-- #primary -->
	</div><!-- #main .wrapper -->
<div id="footer">
  <div id="footerbar">
      <?php if ( is_active_sidebar( 'sidebar-1' ) ) : ?>
          <div id="secondary" class="widget-area" role="complementary">
              <div class="footerbarcontent">
              <?php dynamic_sidebar( 'sidebar-1' ); ?>
              </div>
              <?php if ( is_active_sidebar( 'sidebar-2' ) ) : ?>
                   <div class="footerbarcontent">
                  <?php dynamic_sidebar( 'sidebar-2' ); ?>
                  </div>
                  <?php endif; ?>
                  <?php if ( is_active_sidebar( 'sidebar-3' ) ) : ?>
                   <div class="footerbarcontent">
                      <?php dynamic_sidebar( 'sidebar-3' ); ?>
                  </div>
                  <?php endif; ?>
                  <?php if ( is_active_sidebar( 'sidebar-4' ) ) : ?>
                   <div class="footerbarcontent">
                      <?php dynamic_sidebar( 'sidebar-4' ); ?>
                  </div>
                  <?php endif; ?>
          </div><!-- #secondary -->
      <?php endif; ?>
  </div>
  <div class="clear"></div>
    <div id="footertext">
    <?php echo stripslashes(get_option('mytheme_footertext')); ?>
  </div>
</div>
</div><!-- #page -->
    <div class="sb-slidebar sb-right">
        <?php wp_nav_menu( array('theme_location'  => 'primary','container_class' => 'menu-nav-container','menu_class' => 'menu') ) ;?>
    </div>
   
</div><!-- #container -->
<i class="fa fa-arrow-circle-up back-to-top"></i>
<!-- JS -->
<script type="text/javascript" src="<?php bloginfo('stylesheet_directory') ?>/js/slidebars.min.js"></script>

<!-- Statistical code begin -->
<?php if (get_option('mytheme_analytics')!="") {?>
<div id="analytics"><?php echo stripslashes(get_option('mytheme_analytics')); ?></div>
<?php }?>
<!--Statistical code end-->
<?php wp_footer(); ?>
<link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_directory') ?>/css/audio.css">

 <script>
      (function($) {
        $(document).ready(function() {
          $.slidebars();
        });
      }) (jQuery);
      jQuery(function(){
          // hide #back-top first
          jQuery(".back-to-top").hide();
          // fade in #back-top
          jQuery(function () {
            jQuery(window).scroll(function () {
              if (jQuery(this).scrollTop() > 300) {
                jQuery('.back-to-top').fadeIn();
              } else {
                jQuery('.back-to-top').fadeOut();
              }
            });
            // scroll body to 0px on click
            jQuery('.back-to-top').click(function () {
              jQuery('body,html,header').animate({
                scrollTop: 0
              }, 600);
              return false;
            });
          });
      });
    </script>
</script>
</body>
</html>