<?php if (has_post_thumbnail( $post->ID ) ): ?>
<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?><?php endif; ?>
<div class="postbg" style="background-image: url('<?php echo $image[0]; ?>')">

	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<div class="article">
		<div class="entry-header">
			<div class="fomatlogo">
				<a href="<?php echo get_permalink(); ?>">
					<i class="fa  fa-video-camera fa-3x" title="视频"></i>
				</a>
			</div>
			<h2 class="entry-title">
				<a href="<?php echo get_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
					<?php the_title(); ?>
				</a>
			</h2>
			<div class="post_meta">
				<span class="date"><?php the_time('Y年n月j日') ?></span>
			</div>
		</div>
		<div class="entry-content">
			<div class="video-content">
				<?php echo do_shortcode('[video mp4=' . "'" . get_post_meta( $post->ID, 'video', true ) . "'" . '][/video]'); ?>
			<div class="video-text">
				<?php the_content( __( '<span class="meta-nav" title="继续阅读">继续阅读</span>' )); ?>
			</div>
			</div>
		</div>
		</div>
	</div><!-- end post -->
</div><!-- end bg -->
