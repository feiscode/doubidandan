<?php
/*
Template Name: 归档(按分类)
*/
 get_header(); ?>
<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory') ?>/css/archives.css" type="text/css" media="screen" /> 
			<div class="singlecontent">
				<div class="article">
					<ul class="archive-list">   
					   <?php wp_list_categories('orderby=name&show_count=1&show_last_update=1&use_desc_for_title=1'); ?>
					</ul>
				</div><!-- article -->
			</div><!-- singlecontent -->
			<div class="clear"></div>
<?php get_footer(); ?>