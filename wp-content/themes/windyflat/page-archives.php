<?php
/*
Template Name: 归档
*/
 get_header(); ?>
<link rel="stylesheet" href="<?php bloginfo('stylesheet_directory') ?>/css/archives.css" type="text/css" media="screen" /> 
			<div class="singlecontent">
				<div id="archives" class="article">      
    					<div id="archives-content"> 
					 <form id="archiveform" action="">
						<ul class="years">
						<?php
						$all_posts = get_posts(array(
						  'posts_per_page' => -1 // to show all posts
						));

						// this variable will contain all the posts in a associative array
						// with three levels, for every year, month and posts.

						$ordered_posts = array();

						foreach ($all_posts as $single) {

						  $year  = mysql2date('Y', $single->post_date);
						  $month = mysql2date('F', $single->post_date);

						  // specifies the position of the current post
						  $ordered_posts[$year][$month][] = $single;

						}

						// iterates the years
						foreach ($ordered_posts as $year => $months) { ?>
						  <li>

						    <h2><?php echo $year ?>年</h2>

						    <ul class="months">
						    <?php foreach ($months as $month => $posts ) { // iterates the moths ?>
						      <li>
						        <h3><?php printf("%s (%d篇文章)", $month, count($months[$month])) ?></h3>

						        <ul class="one-article">
						          <?php foreach ($posts as $single ) { // iterates the posts ?>

						            <li>
						              <span class="date"><?php echo mysql2date('Y年n月j日', $single->post_date) ?></span><a href="<?php echo get_permalink($single->ID); ?>" class="article-link"><?php echo get_the_title($single->ID); ?>(<?php echo $single->comment_count ?>条评论)</a></li>
						            </li>

						          <?php } // ends foreach $posts ?>
						        </ul> <!-- ul.posts -->

						      </li>
						    <?php } // ends foreach for $months ?>
						    </ul> <!-- ul.months -->

						  </li> <?php
						} // ends foreach for $ordered_posts
						?>
						</ul><!-- ul.years -->
					</div><!-- archives-content-->
				</div><!-- archives-->
			</div><!-- singlecontent -->

			<div class="clear"></div>

<?php get_footer(); ?>