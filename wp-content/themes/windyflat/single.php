<?php get_header(); ?>
			<div class="singlecontent">
				<div class="article">
					<?php if (have_posts()) : the_post(); update_post_caches($posts); ?>
					<div class="content">
						<h2 class="entry-title">
							<a href="<?php echo get_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
								<?php the_title(); ?>
							</a>
						</h2>
						<div class="post_meta">
							<span class="category"><?php the_category(', '); ?></span>
						</div>
						<?php
						// Must be inside a loop.

						if ( has_post_thumbnail() ) {
							 post_thumbnail(768,260);
						}
						else {
						}
						?>
							<?php $format = get_post_format(); ?>
							<?php if ( $format == "audio" ){ ?>
								<div class="singleplayer">
									<?php echo do_shortcode('[audio mp3=' . "'" . get_post_meta( $post->ID, 'music', true ) . "'" . '][/audio]'); ?>
								</div>		
							<?php } ?>
							<?php if ( $format == "video" ){ ?>
								<div class="videoplayer">
									<?php echo do_shortcode('[video mp4=' . "'" . get_post_meta( $post->ID, 'video', true ) . "'" . '][/video]'); ?>
								</div>		
							<?php } ?>
						<?php the_content(); ?>
						<div class="singlenavi">
							<div class="alignleft">
								<?php previous_post_link(); ?>
							</div>
							<div class="alignright">
								<?php next_post_link(); ?>
							</div>
							<div class="clear"></div>
						</div> 
					</div>

						<?php else : ?>
					    <div class="errorbox">
					        没有文章！
					    </div>
					    <?php endif; ?>
				</div><!-- article -->

			</div><!-- singlecontent -->

			<div class="clear"></div>
			<div class="comment-responsive">
				<?php comments_template(); ?>
			</div>
<?php get_footer(); ?>