<?php 
/*
Template Name: 随机首页
*/
get_header(); ?>
			<div class="content">
			<?php
			query_posts(array('orderby' => 'rand', 'showposts' => 6));
			if (have_posts()) :
			while (have_posts()) : the_post(); ?>
			<?php get_template_part( 'content', get_post_format() ); ?>
			<?php endwhile;
			endif; ?>
				<div class="clear"></div>
				<!-- 分页导航 开始-->
				<div class="footernavi">
					<div class="page_navi"><?php par_pagenavi(5); ?></div>
				</div>
				<!-- 分页导航 结束-->
			</div><!-- #content -->
			<div class="clear"></div>
<?php get_footer(); ?>