<div class="hotposts">
<h3 class="widget-title">最受欢迎</h3>
<ul id="popular-posts">
	<?php 
	// the query
	$the_query = new WP_Query( array(
	    'meta_key' => 'post_views_count',
	    'orderby' => 'meta_value_num',
	    'posts_per_page' => 5,
	    'ignore_sticky_posts' => 1
	) ); ?>
	<?php if ( $the_query->have_posts() ) : ?>
	  <!-- pagination here -->
	  <!-- the loop -->
	  <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
	    <li style="list-style:none">
			<div class="sidebarhotthumb">
				<a class="popular-post-title" href="<?php the_permalink(); ?>" title="<?php echo getPostViews(get_the_ID()); ?>"><?php the_title(); ?></a>
			</div>
		</li>
	  <?php endwhile; ?>
	  <!-- end of the loop -->
	  <!-- pagination here -->
	  <?php wp_reset_postdata(); ?>
	<?php else:  ?>
	  <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
	<?php endif; ?>
</ul>
</div>

