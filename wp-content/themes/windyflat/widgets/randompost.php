
<h3 class="widget-title">随机文章</h3> 

<ul>
<?php
    global $post;
    $postid = $post->ID;

    $args = array( 'orderby' => 'rand', 'post__not_in' => array($post->ID), 'showposts' => 6,'ignore_sticky_posts' => 1);
    $query_posts = new WP_Query();
    $query_posts->query($args);
?>
<?php 
    while ($query_posts->have_posts()) : 
        $query_posts->the_post(); 
?>
<li style="list-style:none">
		<a class="popular-post-title" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
</li>
<?php endwhile; ?>
</ul>
