<?php
 {
    // This theme supports a variety of post formats.
    add_theme_support( 'post-formats', array( 'aside', 'image', 'link', 'gallery','quote', 'status','audio','video'));

   // This theme uses wp_nav_menu() in one location.
    register_nav_menu( 'primary', __( '主菜单', 'widyflat' ) );
    
    // This theme uses a custom image size for featured images, displayed on "standard" posts.
    add_theme_support( 'post-thumbnails' );
    set_post_thumbnail_size( 780, 9999 ); // Unlimited height, soft crop
    function mystyle() {
        wp_enqueue_style( 'slidebars', get_template_directory_uri() . '/css/slidebars.min.css', false, false , false );
        wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css', false, false , false );
}
add_action( 'wp_enqueue_scripts', 'mystyle' );
function myjs() {
        wp_enqueue_script( 'jquery', get_template_directory_uri() . '/js/jquery.min.js', false, false , false );
        wp_enqueue_script( 'nav-js', get_template_directory_uri() . '/js/slidebars.min.js', false, false , false  );
    }
}
add_action( 'wp_enqueue_scripts', 'myjs' );

/* if has shortcode*/
 function galleyfile_scripts() {
        global $post,$posts;
        foreach ($posts as $post) {
           if ( has_shortcode( $post->post_content, 'gallery') ){
                      wp_enqueue_style( 'flexslider', get_template_directory_uri() . '/css/flexslider.css', false, false , false );
                      wp_enqueue_style( 'lightboxstyle', get_template_directory_uri() . '/css/lightbox.css', false, false , false );    
                        wp_enqueue_script( 'flexslider', get_template_directory_uri() . '/js/jquery.flexslider.js', array("jquery"), false , true  );
                    wp_enqueue_script( 'lightbox', get_template_directory_uri() . '/js/lightbox.min.js', array("jquery"), false , true  );
            break;
            };
        }
    }
add_action( 'wp_enqueue_scripts', 'galleyfile_scripts' );

 function music_scripts() {
        global $post,$posts;
        foreach ($posts as $post) {
           if ( has_shortcode( $post->post_content, 'audio') ){
                      wp_enqueue_style('wp-mediaelement');
            break;
            };
        }
    }
add_action( 'wp_enqueue_scripts', 'music_scripts' );


/* title */

if ( ! isset( $content_width ) )
    $content_width = 768;

function my_enqueue_scripts() {
    wp_enqueue_style( 'my-style', get_bloginfo('template_directory').'/css/admin.css' );
}
add_action( 'admin_enqueue_scripts', 'my_enqueue_scripts' );
/*   more */
add_filter('the_content_more_link', 'cp_more_link');
function cp_more_link( $more_link ){
    return "<div class='readmore'>$more_link</div>";
}
 
/*Pagr Navi*/
function par_pagenavi($range = 5){
    global $paged, $wp_query;
    $max_page = $wp_query->max_num_pages;
    if ( !$max_page ) {$max_page = $wp_query->max_num_pages;}
    if($max_page > 1){if(!$paged){$paged = 1;}
    if($paged != 1){echo "<a href='" . get_pagenum_link(1) . "' class='extend' title='跳转到首页'>&laquo;</a>";}
    previous_posts_link('&lsaquo;');
    if($max_page > $range){
        if($paged < $range){for($i = 1; $i <= ($range + 1); $i++){echo "<a href='" . get_pagenum_link($i) ."'";
        if($i==$paged)echo " class='current'";echo ">$i</a>";}}
    elseif($paged >= ($max_page - ceil(($range/2)))){
        for($i = $max_page - $range; $i <= $max_page; $i++){echo "<a href='" . get_pagenum_link($i) ."'";
        if($i==$paged)echo " class='current'";echo ">$i</a>";}}
    elseif($paged >= $range && $paged < ($max_page - ceil(($range/2)))){
        for($i = ($paged - ceil($range/2)); $i <= ($paged + ceil(($range/2))); $i++){echo "<a href='" . get_pagenum_link($i) ."'";if($i==$paged) echo " class='current'";echo ">$i</a>";}}}
    else{for($i = 1; $i <= $max_page; $i++){echo "<a href='" . get_pagenum_link($i) ."'";
    if($i==$paged)echo " class='current'";echo ">$i</a>";}}
    next_posts_link('&rsaquo;');
    if($paged != $max_page){echo "<a href='" . get_pagenum_link($max_page) . "' class='extend' title='跳转到最后一页'>&raquo;</a>";}}
}
if ( ! function_exists( 'windyromantic_comment' ) ) :
/**
 * Template for comments and pingbacks.
 *
 * To override this walker in a child theme without modifying the comments template
 * simply create your own windyromantic_comment(), and that function will be used instead.
 *
 * Used as a callback by wp_list_comments() for displaying the comments.
 *
 * @since Windy Romantic 1.0
 */
function windyromantic_comment( $comment, $args, $depth ) {
    $GLOBALS['comment'] = $comment;
    switch ( $comment->comment_type ) :
        case 'pingback' :
        case 'trackback' :
        // Display trackbacks differently than normal comments.
    ?>
    <li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
        <p><?php _e( 'Pingback:', 'windyromantic' ); ?> <?php comment_author_link(); ?> <?php edit_comment_link( __( '(Edit)', 'windyromantic' ), '<span class="edit-link">', '</span>' ); ?></p>
    <?php
            break;
        default :
        // Proceed with normal comments.
        global $post;
    ?>
    <li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
        <article id="comment-<?php comment_ID(); ?>" class="comment">
            <header class="comment-meta comment-author vcard">
                <?php
                    echo get_avatar( $comment, 44 );
                    printf( '<cite class="fn">%1$s %2$s</cite>',
                        get_comment_author_link(),
                        // If current post author is also comment author, make it known visually.
                        ( $comment->user_id === $post->post_author ) ? '<span> ' . __( 'Post author', 'windyromantic' ) . '</span>' : ''
                    );
                    printf( '<a href="%1$s"><time pubdate datetime="%2$s">%3$s</time></a>',
                        esc_url( get_comment_link( $comment->comment_ID ) ),
                        get_comment_time( 'c' ),
                        /* translators: 1: date, 2: time */
                        sprintf( __( '%1$s at %2$s', 'windyromantic' ), get_comment_date(), get_comment_time() )
                    );
                ?>
            </header><!-- .comment-meta -->

            <?php if ( '0' == $comment->comment_approved ) : ?>
                <p class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'windyromantic' ); ?></p>
            <?php endif; ?>

            <section class="comment-content comment">
                <?php comment_text(); ?>
                <?php edit_comment_link( __( 'Edit', 'windyromantic' ), '<p class="edit-link">', '</p>' ); ?>
            </section><!-- .comment-content -->

            <div class="reply">
                <?php comment_reply_link( array_merge( $args, array( 'reply_text' => __( 'Reply <span>&darr;</span>', 'windyromantic' ), 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
            </div><!-- .reply -->
        </article><!-- #comment-## -->
    <?php
        break;
    endswitch; // end comment_type check
}
endif;


function getPostViews($postID){
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "1 次阅读";
    }
    return $count.' 次阅读 ';
}
function setPostViews($postID) {
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
/*Admin*/

add_action('admin_menu', 'mytheme_page');


function mytheme_page (){
 
    if ( count($_POST) > 0 && isset($_POST['mytheme_settings']) ){
 
        $options = array ('logotitle','description','analytics','bannercolor','bannerstyle','customstyle','footertext');
        foreach ( $options as $opt ){
            delete_option ( 'mytheme_'.$opt, $_POST[$opt] );
            add_option ( 'mytheme_'.$opt, $_POST[$opt] );   
        }
 
    }
    add_theme_page(__('主题选项'), __('主题选项'), 'edit_themes', basename(__FILE__), 'mytheme_settings');
}
function mytheme_settings(){?>
 <div class="wrap">
<div class="childwrap">
 
<h2>主题选项</h2>
 
<form method="post" action="">
 
    <fieldset>
 
    <legend><strong>WindyFlat</strong></legend>
 
        <table class="form-table">
 
            <tr><td>
 
                <textarea name="description" id="description" rows="2" cols="70"><?php echo get_option('mytheme_description'); ?></textarea>
 
                <em>网站描述（Meta Description），针对搜索引擎设置的网页描述。如： WindyLiu部落格</em>
 
            </td></tr>
            
             <tr><td>
 
                <textarea name="logotitle" id="logotitle" rows="1" cols="70"><?php echo get_option('mytheme_logotitle'); ?></textarea>
 
                <em>鼠标悬停在logo头像上的提示语</em>
 
            </td></tr>
            <tr><td>
            <tr><td>
 
                <textarea name="bannercolor" id="bannercolor" rows="1" cols="20"><?php echo get_option('mytheme_bannercolor'); ?></textarea>
 
                <em>自定义Banner颜色</em>
 
            </td></tr>
             <tr><td>

                <textarea name="bannerstyle" id="bannerstyle" rows="1" cols="20"><?php echo get_option('mytheme_bannerstyle'); ?></textarea>
 
                <em>自定义Banner颜色</em>
 
            </td></tr>
            <tr><td>
 
                <textarea name="customstyle" id="customstyle" rows="7" cols="70"><?php echo get_option('mytheme_customstyle'); ?></textarea>
 
                <em>自定义CSS</em>
 
            </td></tr>
            <tr><td>
 
                <textarea name="footertext" id="footertext" rows="2" cols="70"><?php echo stripslashes(get_option('mytheme_footertext')); ?></textarea>
 
                <em>Footer文字</em>
 
            </td></tr>
            <tr><td>
               <form method="post" action="">
                  <input type="radio" name="randomcss" value="yes">
                  <input type="radio" name="randomcss" value="no">
                  <input type="submit">
                </form>
            </td></tr>

        </table>
 
    </fieldset>
 
 
 
    <fieldset>
 
    <legend><strong>统计代码添加</strong></legend>
 
        <table class="form-table">
 
            <tr><td>
 
                <textarea name="analytics" id="analytics" rows="5" cols="70"><?php echo stripslashes(get_option('mytheme_analytics')); ?></textarea>
 
            </td></tr>
 
        </table>
 
    </fieldset>
 
  
 
    <p class="submit">
 
        <input type="submit" name="Submit" class="button-primary" value="保存设置" />
 
        <input type="hidden" name="mytheme_settings" value="save" style="display:none;" />
 
    </p>
 
 
 
</form>
 
</div>
</div>
 
<?php }


/*后台说明*/
function custom_dashboard_help() {
    echo '<p>
            <ol>
                <li>主题包css文件夹内含有duoshuo.css，可以打开复制全部内容到多说自定义CSS选项即可</li>
                <li>音频文章形式请在编辑文章的时候选择“音频”形式，并且在底下输入一个新的自定义字段“music”，值为mp3链接</li>
                <li>视频文章形式请在编辑文章的时候选择“视频”形式，并且在底下输入一个新的自定义字段“video”，值为mp4链接</li>
                <li>相册文章形式请在编辑文章的时候选择“相册”形式，插入多媒体，选择插入相册</li>
                <li>在后台-外观-主题选项中可以自定义Banner的颜色，请输入形如#5EC2A6（默认值）的颜色值</li>
                <li>首页的文章背景即特色图像，请尽量上传大图</li>
                <li>同样在主题选项中可以自定义Footer的文字，请使用HTML代码填写</li>
                <li>新增自定义CSS选项，logo的长阴影可以在 http://sandbox.juan-i.com/longshadows/ 生成，旋转特效也可自行添加</li>
            </ol>
            </p>';

    // 如以下一行代码是露兜博客开放投稿功能所使用的投稿说明
    // echo "<p><ol><li>投稿，请依次点击 文章 - 添加新文章，点击 &quot;送交审查&quot; 即可提交</li><li>修改个人资料，请依次点击 资料 - 我的资料</li><li>请认真填写“个人说明”，该信息将会显示在文章末尾</li><li>有事请与我联系，Email: zhouzb889@gmail.com&nbsp;&nbsp;&nbsp;QQ: 825533758</li></ol></p>";     
}

function example_add_dashboard_widgets() {
    wp_add_dashboard_widget('custom_help_widget', '后台帮助,重要!', 'custom_dashboard_help');
}
add_action('wp_dashboard_setup', 'example_add_dashboard_widgets' );
/* 邮件通知 by Qiqiboy */
 function comment_mail_notify($comment_id) {
     $comment = get_comment($comment_id);//根据id获取这条评论相关数据
     $content=$comment->comment_content;
     //对评论内容进行匹配
     $match_count=preg_match_all('/<a href="#comment-([0-9]+)?" rel="nofollow">/si',$content,$matchs);
     if($match_count>0){//如果匹配到了
         foreach($matchs[1] as $parent_id){//对每个子匹配都进行邮件发送操作
             SimPaled_send_email($parent_id,$comment);
         }
     }elseif($comment->comment_parent!='0'){//以防万一，有人故意删了@回复，还可以通过查找父级评论id来确定邮件发送对象
         $parent_id=$comment->comment_parent;
         SimPaled_send_email($parent_id,$comment);
     }else return;
 }
 add_action('comment_post', 'comment_mail_notify');
 function SimPaled_send_email($parent_id,$comment){//发送邮件的函数 by Qiqiboy.com
     $admin_email = get_bloginfo ('admin_email');//管理员邮箱
     $parent_comment=get_comment($parent_id);//获取被回复人（或叫父级评论）相关信息
     $author_email=$comment->comment_author_email;//评论人邮箱
     $to = trim($parent_comment->comment_author_email);//被回复人邮箱
     $spam_confirmed = $comment->comment_approved;
     if ($spam_confirmed != 'spam' && $to != $admin_email && $to != $author_email) {
         $wp_email = 'no-reply@' . preg_replace('#^www\.#', '', strtolower($_SERVER['SERVER_NAME'])); // e-mail 發出點, no-reply 可改為可用的 e-mail.
         $subject = '您在 [' . get_option("blogname") . '] 的留言有了回應';
         $message = '<div style="background-color:#eef2fa;border:1px solid #d8e3e8;color:#111;padding:0 15px;-moz-border-radius:5px;-webkit-border-radius:5px;-khtml-border-radius:5px;">
             <p>' . trim(get_comment($parent_id)->comment_author) . ', 您好!</p>
             <p>您曾在《' . get_the_title($comment->comment_post_ID) . '》的留言:<br />'
             . trim(get_comment($parent_id)->comment_content) . '</p>
             <p>' . trim($comment->comment_author) . ' 给你的回复:<br />'
             . trim($comment->comment_content) . '<br /></p>
             <p>您可以点击 <a href="' . htmlspecialchars(get_comment_link($parent_id,array("type" => "all"))) . '">查看回复的完整內容</a></p>
             <p>欢迎再度光临 <a href="' . get_option('home') . '">' . get_option('blogname') . '</a></p>
             <p>(此邮件有系统自动发出, 请勿回复.)</p></div>';
         $from = "From: \"" . get_option('blogname') . "\" <$wp_email>";
         $headers = "$from\nContent-Type: text/html; charset=" . get_option('blog_charset') . "\n";
         wp_mail( $to, $subject, $message, $headers );
     }
 }
 /*TimThumb*/
define('TIMTHUMBSITE', site_url());
 /*移除字体*/
function remove_open_sans() {
wp_deregister_style( 'open-sans' );
wp_register_style( 'open-sans', false );
wp_enqueue_style('open-sans','');
}
add_action( 'init', 'remove_open_sans' );
/*手机判断*/
function is_mobile() {
    $user_agent = $_SERVER['HTTP_USER_AGENT'];
    $mobile_browser = Array(
        "mqqbrowser", //手机QQ浏览器
        "opera mobi", //手机opera
        "juc","iuc",//uc浏览器
        "fennec","ios","applewebKit/420","applewebkit/525","applewebkit/532","ipad","iphone","ipaq","ipod",
        "iemobile", "windows ce",//windows phone
        "240x320","480x640","acer","android","anywhereyougo.com","asus","audio","blackberry","blazer","coolpad" ,"dopod", "etouch", "hitachi","htc","huawei", "jbrowser", "lenovo","lg","lg-","lge-","lge", "mobi","moto","nokia","phone","samsung","sony","symbian","tablet","tianyu","wap","xda","xde","zte"
    );
    $is_mobile = false;
    foreach ($mobile_browser as $device) {
        if (stristr($user_agent, $device)) {
            $is_mobile = true;
            break;
        }
    }
    return $is_mobile;
}
/*缩略图*/
function post_thumbnail( $width = 768,$height = 500 ){
 global $post;
 if( has_post_thumbnail() ){ //如果有缩略图，则显示缩略图
 $timthumb_src = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID),'full');
 $post_timthumb = '<img src="'.get_bloginfo("template_url").'/timthumb/timthumb.php?src='.$timthumb_src[0].'&amp;h='.$height.'&amp;w='.$width.'&amp;zc=1" alt="'.$post->post_title.'" class="thumb" />';
 echo $post_timthumb;
 }
}




/*底栏*/

function windyflat_widgets_init() {
    register_sidebar( array(
        'name' => __( '第一底边栏', 'windyflat' ),
        'id' => 'sidebar-1',
        'description' => __( '左边的小工具', 'windyflat' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ) );

    register_sidebar( array(
        'name' => __( '第二底边栏', 'windyflat' ),
        'id' => 'sidebar-2',
        'description' => __( '中间的小工具', 'windyflat' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>'
    ) );

    register_sidebar( array(
        'name' => __( '第三底边栏', 'windyflat' ),
        'id' => 'sidebar-3',
        'description' => __( '右边的小工具', 'windyflat' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>'
    ) );
}
add_action( 'widgets_init', 'windyflat_widgets_init' );


 // Register Sidebar
if ( function_exists('register_sidebar') )

    register_sidebar(array(

        'name'=>'Sidebar',

        'before_widget' => '<div class="widget">',

        'after_widget' => '</div>',

        'before_title' => '<h6>',

        'after_title' => '</h6>',

));



/*热门*/
function popularPosts($num) {  
    global $wpdb;  
      
    $posts = $wpdb->get_results("SELECT comment_count, ID, post_title FROM $wpdb->posts ORDER BY comment_count DESC LIMIT 0 , $num");  
      
    foreach ($posts as $post) {  
        setup_postdata($post);  
        $id = $post->ID;  
        $title = $post->post_title;  
        $count = $post->comment_count;  
          
        if ($count != 0) {  
            $popular .= '<li>';  
            $popular .= '<a href="' . get_permalink($id) . '" title="' . $title . '">' . $title . '</a> ';
            $popular .= '</li>';  
        }  
    }  
    return $popular;  
}
//小工具定义
if( function_exists( 'register_sidebar_widget' ) ) {   
    register_sidebar_widget('最受欢迎','funny_hot');   
}  
function funny_hot() { include(TEMPLATEPATH . '/widgets/hot.php'); }   

if( function_exists( 'register_sidebar_widget' ) ) {   
    register_sidebar_widget('随机文章','randompost');
}  
function randompost() { include(TEMPLATEPATH . '/widgets/randompost.php'); }   

/*灯箱*/
//deactivate WordPress function
remove_shortcode('gallery', 'gallery_shortcode');
//activate own function
add_shortcode('gallery', 'my_gallery_shortcode');
  function add_fancybox_rel ($content){
    global $post;
      $pattern = "/<a(.*?)href=('|\")([^>]*).(bmp|gif|jpeg|jpg|png)('|\")(.*?)>(.*?)<\/a>/i";
      $replacement = '<li><a $1href=$2$3.$4$5 class="swipebox" rel="gallery-1' .'"$6>$7</a> </li>';
      $content = preg_replace($pattern, $replacement, $content);
      return $content;
  }
  add_filter('wp_get_attachment_link', 'add_fancybox_rel', 12);
function my_gallery_shortcode($attr) {
$post = get_post();
 
if ( ! empty( $attr['ids'] ) ) {
// 'ids' is explicitly ordered, unless you specify otherwise.
if ( empty( $attr['orderby'] ) )
$attr['orderby'] = 'post__in';
$attr['include'] = $attr['ids'];
}
 
// Allow plugins/themes to override the default gallery template.
$output = apply_filters('post_gallery', '', $attr);
if ( $output != '' )
return $output;
 
// We're trusting author input, so let's at least make sure it looks like a valid orderby statement
if ( isset( $attr['orderby'] ) ) {
$attr['orderby'] = sanitize_sql_orderby( $attr['orderby'] );
if ( !$attr['orderby'] )
unset( $attr['orderby'] );
}
 
extract(shortcode_atts(array(
'order' => 'ASC',
'orderby' => 'menu_order ID',
'id' => $post->ID,
'size' => 'full',
'include' => '',
'exclude' => ''
), $attr));
 
$id = intval($id);
if ( 'RAND' == $order )
$orderby = 'none';
 
if ( !empty($include) ) {
$_attachments = get_posts( array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
 
$attachments = array();
foreach ( $_attachments as $key => $val ) {
$attachments[$val->ID] = $_attachments[$key];
}
} elseif ( !empty($exclude) ) {
$attachments = get_children( array('post_parent' => $id, 'exclude' => $exclude, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
} else {
$attachments = get_children( array('post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
}
 
if ( empty($attachments) )
return '';

$output .= "<div class='flexslider'><ul class='slides'>";
foreach ( $attachments as $id => $attachment ) {
$link   = wp_get_attachment_url( $id );
$output .= '<li>'.'<a href="'.$link.'" target="_blank"  data-lightbox="roadtrip"><img src="'.get_bloginfo("template_url").'/timthumb/timthumb.php?src='.$link.'&h=495&w=760&zc=1'.'"/></a></li>';
}
$output .= " </ul></div>";
 
return $output;
}


 /*** AJAX评论 ***/

// 评论回复构架
function themecomment($comment, $args, $depth) {
   $GLOBALS['comment'] = $comment;
        global $commentcount;
        if(!$commentcount) {
           $page = ( !empty($in_comment_loop) ) ? get_query_var('cpage')-1 : get_page_of_comment( $comment->comment_ID, $args )-1;
           $cpp = get_option('comments_per_page');
           $commentcount = $cpp * $page;
        }
        /* 区分普通评论和Pingback */
        switch ($pingtype=$comment->comment_type) {
        case 'pingback' : /* 标识Pingback */
        case 'trackback' : /* 标识Trackback */

?>

<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
    <div id="comment-<?php comment_ID(); ?>">
        <div class="comment-author vcard pingback">
            <cite class="fn zborder_pingback"><?php comment_date('Y-m-d') ?> &raquo; <?php comment_author_link(); ?></cite>
        </div>
    </div>

    <?php
        break;
        /* 标识完毕 */
        default : /* 普通评论部分 */ 
        if(!$comment->comment_parent){ ?>

<li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">

<div class="clear"></div>
<article id="comment-<?php comment_ID(); ?>" class="comment-body">
<div class="comment-body-header">
<?php echo get_avatar( $comment, $size = '60'); ?>
</div>
<section class="comment-content">
<header class="comment-header">
<span class="comment-author"><?php printf( __( '<cite class="fn">%s</cite>'), get_comment_author_link() ); ?>
<span class="datetime"><?php comment_date('Y-m-d') ?> <?php comment_time() ?> </span>
</header>
<div class="commenttext">
    <?php comment_text(); ?>
</div>
<span class="reply"><?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth'], 'reply_text' => __('回复')))) ?></span></span>
</section>  
</article>
<div class="clear"></div>
<?php }else{?>

<li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">

<article id="comment-<?php comment_ID(); ?>" class="comment-body comment-children-body">

<div class="comment-body-header">
<?php echo get_avatar( $comment, $size = '40'); ?>
</div>
<section class="comment-content">
<header class="comment-header">
<span class="comment-author"><?php $parent_id = $comment->comment_parent; $comment_parent = get_comment($parent_id); printf(__('%s'), get_comment_author_link()) ?> 回复 <a href="<?php echo "#comment-".$parent_id;?>" title="<?php echo mb_strimwidth(strip_tags(apply_filters('the_content', $comment_parent->comment_content)), 0, 100,"..."); ?>"><?php echo $comment_parent->comment_author;?></a></span>
<span class="datetime"><?php comment_date('Y-m-d') ?> <?php comment_time() ?> </span>
</header>
<div class="clear"></div>
<div class="commenttext">
    <?php comment_text(); ?>
</div>
<span class="floor"><?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth'], 'reply_text' => __('回复')))) ?></span>
</section>  
</article>
<div class="clear"></div>
<?php }
break; /* 普通评论标识完毕 */
    }
}

//comment_popup_links只统计评论数
if (function_exists('wp_list_comments')) {
    // comment count
    add_filter('get_comments_number', 'comment_count', 0);
    function comment_count( $commentcount ) {
        global $id;
        $_commnets = get_comments('post_id=' . $id);
        $comments_by_type = &separate_comments($_commnets);
        return count($comments_by_type['comment']);
    }
}


function ajax_comment_scripts() {
    global $pagenow;
    if(is_singular()){
        wp_enqueue_script( 'base', get_template_directory_uri() . '//js/comments-ajax.js', array(), '1.00', true);
        wp_localize_script('base', 'bigfa_Ajax_Url', array(       
        "um_ajaxurl" => admin_url('admin-ajax.php')
        ));
    }
}
add_action('wp_enqueue_scripts', 'ajax_comment_scripts');
add_action('wp_ajax_nopriv_ajax_comment', 'ajax_comment');
add_action('wp_ajax_ajax_comment', 'ajax_comment');
function ajax_comment(){
    global $wpdb;
    $comment_post_ID = isset($_POST['comment_post_ID']) ? (int) $_POST['comment_post_ID'] : 0;
    $post = get_post($comment_post_ID);
    if ( empty($post->comment_status) ) {
        do_action('comment_id_not_found', $comment_post_ID);
        ajax_comment_err(__('Invalid comment status.'));
    }
    $status = get_post_status($post);
    $status_obj = get_post_status_object($status);
    if ( !comments_open($comment_post_ID) ) {
        do_action('comment_closed', $comment_post_ID);
        ajax_comment_err(__('Sorry, comments are closed for this item.'));
    } elseif ( 'trash' == $status ) {
        do_action('comment_on_trash', $comment_post_ID);
        ajax_comment_err(__('Invalid comment status.'));
    } elseif ( !$status_obj->public && !$status_obj->private ) {
        do_action('comment_on_draft', $comment_post_ID);
        ajax_comment_err(__('Invalid comment status.'));
    } elseif ( post_password_required($comment_post_ID) ) {
        do_action('comment_on_password_protected', $comment_post_ID);
        ajax_comment_err(__('Password Protected'));
    } else {
        do_action('pre_comment_on_post', $comment_post_ID);
    }
    $comment_author       = ( isset($_POST['author']) )  ? trim(strip_tags($_POST['author'])) : null;
    $comment_author_email = ( isset($_POST['email']) )   ? trim($_POST['email']) : null;
    $comment_author_url   = ( isset($_POST['url']) )     ? trim($_POST['url']) : null;
    $comment_content      = ( isset($_POST['comment']) ) ? trim($_POST['comment']) : null;
    $user = wp_get_current_user();
    if ( $user->exists() ) {
        if ( empty( $user->display_name ) )
            $user->display_name=$user->user_login;
        $comment_author       = $wpdb->escape($user->display_name);
        $comment_author_email = $wpdb->escape($user->user_email);
        $comment_author_url   = $wpdb->escape($user->user_url);
        $user_ID              = $wpdb->escape($user->ID);
        if ( current_user_can('unfiltered_html') ) {
            if ( wp_create_nonce('unfiltered-html-comment_' . $comment_post_ID) != $_POST['_wp_unfiltered_html_comment'] ) {
                kses_remove_filters();
                kses_init_filters();
            }
        }
    } else {
        if ( get_option('comment_registration') || 'private' == $status )
            ajax_comment_err(__('Sorry, you must be logged in to post a comment.'));
    }
    $comment_type = '';
    if ( get_option('require_name_email') && !$user->exists() ) {
        if ( 6 > strlen($comment_author_email) || '' == $comment_author )
            ajax_comment_err( __('Error: please fill the required fields (name, email).') );
        elseif ( !is_email($comment_author_email))
            ajax_comment_err( __('Error: please enter a valid email address.') );
    }
    if ( '' == $comment_content )
        ajax_comment_err( __('Error: please type a comment.') );
    $dupe = "SELECT comment_ID FROM $wpdb->comments WHERE comment_post_ID = '$comment_post_ID' AND ( comment_author = '$comment_author' ";
    if ( $comment_author_email ) $dupe .= "OR comment_author_email = '$comment_author_email' ";
    $dupe .= ") AND comment_content = '$comment_content' LIMIT 1";
    if ( $wpdb->get_var($dupe) ) {
        ajax_comment_err(__('Duplicate comment detected; it looks as though you&#8217;ve already said that!'));
    }
    if ( $lasttime = $wpdb->get_var( $wpdb->prepare("SELECT comment_date_gmt FROM $wpdb->comments WHERE comment_author = %s ORDER BY comment_date DESC LIMIT 1", $comment_author) ) ) {
        $time_lastcomment = mysql2date('U', $lasttime, false);
        $time_newcomment  = mysql2date('U', current_time('mysql', 1), false);
        $flood_die = apply_filters('comment_flood_filter', false, $time_lastcomment, $time_newcomment);
        if ( $flood_die ) {
            ajax_comment_err(__('You are posting comments too quickly.  Slow down.'));
        }
    }
    $comment_parent = isset($_POST['comment_parent']) ? absint($_POST['comment_parent']) : 0;
    $commentdata = compact('comment_post_ID', 'comment_author', 'comment_author_email', 'comment_author_url', 'comment_content', 'comment_type', 'comment_parent', 'user_ID');

    $comment_id = wp_new_comment( $commentdata );

    $comment = get_comment($comment_id);
    do_action('set_comment_cookies', $comment, $user);
    $comment_depth = 1;
    $tmp_c = $comment;
    while($tmp_c->comment_parent != 0){
        $comment_depth++;
        $tmp_c = get_comment($tmp_c->comment_parent);
    }
    $GLOBALS['comment'] = $comment; //your comments here    edit start 
        if(!$comment->comment_parent){
        //以下是評論式樣, 不含 "回覆". 要用你模板的式樣 copy 覆蓋.
?>
<li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">

<article id="comment-<?php comment_ID(); ?>" class="comment-body comment-children-body">

<div class="comment-body-header">
<?php echo get_avatar( $comment, $size = '40'); ?>
</div>
<section class="comment-content">
<header class="comment-header">
<span class="comment-author"><?php $parent_id = $comment->comment_parent; $comment_parent = get_comment($parent_id); printf(__('%s'), get_comment_author_link()) ?> 回复 <a href="<?php echo "#comment-".$parent_id;?>" title="<?php echo mb_strimwidth(strip_tags(apply_filters('the_content', $comment_parent->comment_content)), 0, 100,"..."); ?>"><?php echo $comment_parent->comment_author;?></a></span>
<span class="datetime"><?php comment_date('Y-m-d') ?> <?php comment_time() ?> </span>
</header>
<div class="commenttext">
    <?php comment_text(); ?>
</div>
</section>  
</article>
<div class="clear"></div>

<?php }else{?>

<li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">

<div class="clear"></div>
<article id="comment-<?php comment_ID(); ?>" class="comment-body">
<div class="comment-body-header">
<?php echo get_avatar( $comment, $size = '40'); ?>
</div>
<section class="comment-content">
<header class="comment-header">
<span class="comment-author"><?php printf( __( '<cite class="fn">%s</cite>'), get_comment_author_link() ); ?>
<span class="datetime"><?php comment_date('Y-m-d') ?> <?php comment_time() ?> </span>
</header>
<div class="commenttext">
    <?php comment_text(); ?>
</div>
</section>  
</article>
<div class="clear"></div>

        <?php } die();

}
function ajax_comment_err($a) {
    header('HTTP/1.0 500 Internal Server Error');
    header('Content-Type: text/plain;charset=UTF-8');
    echo $a;
    exit;
}

//Comments Ajax end.

/*///////////////////////*/
add_action('wp_ajax_nopriv_ajax_comment_page_nav', 'ajax_comment_page_nav');
add_action('wp_ajax_ajax_comment_page_nav', 'ajax_comment_page_nav');
function ajax_comment_page_nav(){
    global $post,$wp_query, $wp_rewrite;
    $postid = $_POST["um_post"];
    $pageid = $_POST["um_page"];
    $comments = get_comments('post_id='.$postid);
    $post = get_post($postid);
    if( 'desc' != get_option('comment_order') ){
        $comments = array_reverse($comments);
    }
    $wp_query->is_singular = true;
    $baseLink = '';
    if ($wp_rewrite->using_permalinks()) {
        $baseLink = '&base=' . user_trailingslashit(get_permalink($postid) . 'comment-page-%#%', 'commentpaged');
    }
    echo '<ol class="commentlist">';
    wp_list_comments('type=comment&callback=themecomment&max_depth=500&page=' . $pageid . '&per_page=' . get_option('comments_per_page'), $comments);//注意修改mycomment这个callback
    echo '</ol>';
    echo '<div id="commentnav" data-post-id="'.$postid.'">';
    paginate_comments_links('current=' . $pageid . '&prev_text=« Prev&next_text=Next »');
    echo '</div>';
    die;
}?>
