# coding: utf-8

import urllib2
import re
import HTMLParser
import sys


def getHtml(url):
    header = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:14.0) Gecko/20100101 Firefox/14.0.1',
              'Referer': '******'}
    request = urllib2.Request(url, None, header)
    response = urllib2.urlopen(request)
    text = response.read()
    return text


def getUrls(html):
    pattern = re.compile(r"kp_news_id=(\d+)", re.S)
    items = re.findall(pattern, html)
    urls = []
    for item in items:
        urls.append('http://km.netease.com/kp_news/view?kp_news_id=' + item)
    for x in urls:
        while urls.count(x)>1:
            del urls[urls.index(x)]
    return urls[0]

def getContent(url):
    html = getHtml(url)
    pattern = re.compile('KM独家：游戏那点事儿（(.*?)）')
    date = re.findall(pattern, html)
    print("逗比蛋蛋 （%s）" % date[0])
    htmlParser = HTMLParser.HTMLParser()
    pattern = re.compile('<p>(.*?)悠悠(.*?)|(.*?)妮妮(.*?)</p>')
    items = re.findall(pattern, html)
    result = []

    return items


def main():
    url = "http://km.netease.com/kp_news/category?category_id=346"
    html = getHtml(url)
    urls = getUrls(html)
    t = getContent(urls)
    print(t)

if __name__ == "__main__":
    main()
